# WFR Stats

**This script is now part of the [Wayfarer Extension Scripts](https://github.com/AlterTobi/Wayfarer-Extension-Scripts)**


## important
- not fully functionally since Wayfarer changed it's structure and programming

## what is it?
Userscript for [NIANTIC WAYFARER](https://wayfarer.nianticlabs.com/)

> Userscript manager such as [Tampermonkey](https://tampermonkey.net/) required!
> Report bugs and/or feature requests in the [Issue Tracker](https://gitlab.com/fotofreund0815/opr-stats/issues)

> **Install/Download:** https://gitlab.com/fotofreund0815/opr-stats/raw/master/wfr-stats.user.js

## Features:
- save your daily Wayfarer progress in browser storage
- display your stats, daily and weekly progress
- count and shows occurrence of Pokémon Go / HPWU related descriptions
- display heatmap of your recent reviews

![](./image/opr-stats.png)
![](./image/opr-pogo.png)
